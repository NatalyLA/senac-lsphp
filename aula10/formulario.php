<?php

    $usuario_bd = "root";
    $senha_bd = "";
    $ip_bd = "localhost";
    $porta_bd = 3307;
    $nome_bd = "aula_php";

    /** 
     * CONECTANDO COM O BANCO DE DADOS 
    */
    if($db = mysqli_connect ($ip_bd, $usuario_bd, $senha_bd, $nome_bd, $porta_bd)){
        
    }else{
        die ("Problema ao conectar ao SGBD");
    }



    /** 
     * OBTENDO OS DADOS DO USUARIO INFORMADO NO FORM (READ) 
    */
    if(isset($_POST['email']) && isset($_POST['password'])){
        $prep = mysqli_prepare($db, 'SELECT id, nome, email, senha FROM tb_usuario WHERE senha = ? AND email = ?'); 
        mysqli_stmt_bind_param($prep,'ss', $_POST['password'] ,$_POST['email']);
        mysqli_stmt_execute($prep);

        $result = mysqli_stmt_get_result($prep);
        $usuario = $result -> fetch_assoc();
        
        /**
         * VERIFICANDO SE AS CREDENCIAIS DO USUARIO SÃO VALIDAS 
         * E SE FOREM ENCONTRADAS NO BANCO DE DADOS
         */
        if ($usuario != null){
            session_start();

            $_SESSION['id_usuario'] = $usuario['id'];
            $_SESSION['nome_usuario'] = $usuario['nome'];

            require('menu.php');
            exit();

        }else{
            echo 'Login ou senha inválido';
        }
    }

    echo "
    <form method='POST'>
        E-mail: <input type = 'text' name = 'email'>
        Senha: <input type = 'password' name = 'password'>
        <input type = 'submit' value = 'Entrar'>
    </form>";

