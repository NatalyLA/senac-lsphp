<?php
class Usuario {
    private $id;
    private $nome;
    private $senha;
    private $email;

    public function setId(int $id){
        $this->$id = $id;
    }

    public function setNome(string $nome){
        $this->$nome = $nome;
    }

    public function setSenha(string $senha){
        $this->$senha = $senha;
    }

    public function setEmail(string $email){
        $this->$email = $email;
    }

    public function getId() : int{
        return $this->$id;
    }

    public function getNome() : string{
        return $this->$nome;
    }

    public function getSenha() : string{
        return $this->$senha;
    }

    public function getEmail() : string{
        return $this->$email;
    }
}