<?php
class Usuario {
    private $id;
    private $nome;
    private $senha;
    private $email;
    private $objDb;

    public function __construct(){
        $this->objDb = new mysqli('localhost','root','','aula_php',3307);
    }
    
    public function setId(int $id){
        $this->id = $id;
    }

    public function setNome(string $nome){
        $this->nome = $nome;
    }

    public function setSenha(string $senha){
        $this->senha = $senha = password_hash($senha, PASSWORD_DEFAULT);
    }

    public function setEmail(string $email){
        $this->email = $email;
    }

    public function getId() : int{
        return $this->id;
    }

    public function getNome() : string{
        return array ($this->id, $this->nome, $this->email);
    }

    public function getEmail() : string{
        return array ($this->id, $this->nome, $this->email);
    }

    public function getSenha() : string{
        return array ($this->id, $this->nome, $this->email);
    }

    public function saveUsuario(){
    
        $objStmt = $this->objDb->prepare('REPLACE INTO tb_usuario(id, nome, email, senha) VALUES (?,?,?,?)');
        $objStmt->bind_param('isss', $this->id, $this->nome, $this->email, $this->senha);

        return $objStmt-> execute();
    }

    public function deleteUsuario(){
        $objStmt = $this->objDb->prepare('DELETE FROM tb_usuario WHERE id = ?');
        $objStmt->bind_param('i', $this->id);

        return $objStmt-> execute();
    } 

    
    public function findUsuarios(){
        $query = "SELECT id, nome, email, senha FROM tb_usuario";
        if ($result = $this->objDb->query($query)) {
            while ($row = $result->fetch_row()) {
                printf("%s (%s,%s)\n", $row[0], $row[1], $row[2]);
            }
            $result->close();
        }
    }

    public function __destruct(){
        unset($this->objDb);
    }
}