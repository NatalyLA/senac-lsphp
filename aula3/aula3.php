<?php
   echo "<pre>";

    //constante no PHP
    define ('QTD_PAGINAS', 10);

    echo "O valor da minha constante é: " .QTD_PAGINAS;

    //variavel para passar o valor para uma constante
    $ip_do_bd = '192.168.45.12';

    define ('IP_DO_BD', $ip_do_bd);

    echo "\nO IP do SGBD é: " .IP_DO_BD;
    
    //Constantes magicas
    echo "\nEstou na linha: " .__LINE__;
    echo "\nEstou na linha: " .__LINE__;
    echo "\nEste é o arquivo: " .__FILE__;



    //bom para depurar o código
    echo "\n\n";
    var_dump ($ip_do_bd);

/* 
 * Agora fica mais legal 
 * é a hora do vetor!
 * Array
*/ 

/*$dias_da_semana [0] = 'dom', 
$dias_da_semana [1] = 'seg',
$dias_da_semana [2] = 'ter',
$dias_da_semana [3] = 'qua',
$dias_da_semana [4] = 'qui',
$dias_da_semana [5] = 'sex',
$dias_da_semana [6] = 'sab';*/

var_dump($dias_da_semana);
//unset ($dias_da_semana); //destroi variavel

$dias_da_semana = array (0 => 'dom',
                         1 => 'seg',
                         2 => 'ter',    
                         3 => 'qua',
                         4 => 'qui',
                         5 => 'sex',
                         6 => 'sab');
echo "\n\n";

var_dump($dias_da_semana);



echo "</pre>";

?>