<?php

    // Mostra detalhes do ambiente
    //phpinfo();

   // echo "Seu IP é: " .$_SERVER['REMOTE_ADDR'];
    
    echo "Seu IP é:  {$_SERVER['REMOTE_ADDR']}";

    //Vetor com mais de uma dimensão

   /* $aluno [0] ['nome'] = 'Joao Silva';
    $aluno [0] ['bitbucket'] = 'https://bitbucket...';
    $aluno [1] ['nome'] = 'Julia Almeida';
    $aluno [1] ['bitbucket'] = 'https://bitbucket...';
    $aluno [2] ['nome'] = 'Ricardo Leite';
    $aluno [2] ['bitbucket'] = 'https://bitbucket...';
    $aluno [3] ['nome'] = 'Gabriel Jesus';
    $aluno [3] ['bitbucket'] = 'https://bitbucket...';
    $aluno [4] ['nome'] = 'Adriano Imperador';
    $aluno [4] ['bitbucket'] = 'https://bitbucket...';
    $aluno [5] ['nome'] = 'Ronaldinho Gacho';
    $aluno [5] ['bitbucket'] = 'https://bitbucket...';
   
    var_dump($aluno); 
    */
    
// outra forma
    $aluno = array(0 => array ('nome' => 'Joao Silva',
                           'bitbucket' => 'https://bitbucket...'),
                   1 => array ('nome' => 'Julia Almeida',
                           'bitbucket' => 'https://bitbucket...'),
                   2 => array ('nome' => 'Ricardo Leite',
                           'bitbucket' => 'https://bitbucket...'),
                   3 => array ('nome' => 'Gabriel Jesus',
                           'bitbucket' => 'https://bitbucket...'),
                   4 => array ('nome' => 'Adriano Imperador',
                           'bitbucket' => 'https://bitbucket...'),
                   5 => array ('nome' => 'Ronaldinho Gacho',
                           'bitbucket' => 'https://bitbucket...'));

      var_dump($aluno); 


 



?>