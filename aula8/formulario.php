<?php

    $usuario_bd = "root";
    $senha_bd = "";
    $ip_bd = "localhost";
    $porta_bd = 3307;
    $nome_bd = "aula_php";

    /** 
     * CONECTANDO COM O BANCO DE DADOS 
    */
    if($db = mysqli_connect ($ip_bd, $usuario_bd, $senha_bd, $nome_bd, $porta_bd)){
        
    }else{
        die ("Problema ao conectar ao SGBD");
    }


    echo "
    <form method='POST'>
        E-mail: <input type = 'text' name = 'email'>
        Senha: <input type = 'password' name = 'password'>
        <input type = 'submit' value = 'Entrar'>
    </form>";



    /** 
     * OBTENDO OS DADOS DO USUARIO INFORMADO NO FORM (READ) 
    */
    if(isset($_POST['email']) && isset($_POST['password'])){
        $prep = mysqli_prepare($db, 'SELECT nome, email, senha FROM tb_usuario WHERE senha = ? AND email = ?'); 
        mysqli_stmt_bind_param($prep,'ss', $_POST['password'] ,$_POST['email']);
        mysqli_stmt_execute($prep);
        $result = mysqli_stmt_get_result($prep);
        $usuario = $result -> fetch_assoc();
        
        $login = false;
        if ($usuario != null){
            $login = true;
            echo 'Entrou';
        }
    }
    
    if ($login === false){
        echo 'Login ou senha inválido';
    }

?>